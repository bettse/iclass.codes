import React, { useState, useEffect } from "react";

import CryptoJS from 'crypto-js';
import { When } from 'react-if';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Alert from 'react-bootstrap/Alert';

import './App.css';
import flag from './flag.png';

const rawKeyUrl = 'https://raw.githubusercontent.com/shingliao/proxmark3/94522e41972a16c170a3df05ed5343d6b24c0496/client/resources/iclass_decryptionkey.bin';

const options = {
  iv: CryptoJS.enc.Hex.parse('00000000000000000000000000000000'),
  padding: CryptoJS.pad.NoPadding,
  mode: CryptoJS.mode.ECB,
};

function App() {
  const [key, setKey] = useState(null);
  const [hexInput, setHexInput] = useState('');
  const [inputSansSpace, setInputSansSpace] = useState('');
  const [hexOutput, setHexOutput] = useState('');

  useEffect(() => {
    async function fetchKey() {
      try {
        const response = await fetch(rawKeyUrl);
        if (response.ok) {
          const text = await response.blob();
          const blob = new Blob([text], {type: 'application/octet-stream'});
          const arrayBuffer = await blob.arrayBuffer()
          setKey(arrayBuffer);
        } else {
          console.log('response not ok');
        }
      } catch (e) {
        console.log(e)
      }
    }
    fetchKey();
  }, [])

  useEffect(() => {
    if (key) {
      const keyWords = CryptoJS.lib.WordArray.create(key)

      const ciphertext = CryptoJS.enc.Hex.parse(inputSansSpace.replace(' ', '').replace(',', ''))

      const text = CryptoJS.lib.CipherParams.create({ciphertext})
      const bytes = CryptoJS.TripleDES.decrypt(text, keyWords, options);

      const clear = bytes.toString(CryptoJS.enc.Hex);
      setHexOutput(clear);
    }
  }, [key, inputSansSpace])

  useEffect(() => {
    setInputSansSpace(hexInput.split(" ").join(""))
  }, [key, hexInput])

  return (
    <div className="App">
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Brand>
          <Image thumbnail style={{width: '10rem', height: 'auto'}} src={flag} alt="Flag" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
          </Nav>
          <Nav>
            <Navbar.Text>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container className="text-center pt-5" fluid='md'>
        <Row className="justify-content-center text-center">
          <Col xs={3}>
            <p>Block 7 in hexadecimal (spaces will be removed)</p>
            <Form.Control type="text" name="block7" onChange={(event) => {setHexInput(event.target.value)}} />

            <When condition={hexOutput.length > 0}>
              <Alert className="mt-5" variant="success">{hexOutput}</Alert>
            </When>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
